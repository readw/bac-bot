//const Discord = require("discord.io");
const Discord = require("discord.js");
const Logger = require("./Core/Logger");
const CommandFactory = require("./Core/CommandFactory");

class BacBot {

    constructor()
    {
        this.bot = new Discord.Client();
        this.logger = new Logger();
    }

    ready() {
        this.bot.on("ready", (event) => {
            this.logger.Debug(`Logged in as: ${this.bot.user.username} - (${this.bot.user.id})`);
        });
        this.bot.login(process.env.TOKEN);
    }

    message() {
        this.bot.on("message", (message) => {
            if (message.author.bot) { return; }
            var commandFactory = new CommandFactory(this.bot, message);
            if (message.content.substring(0, 6).toLocaleLowerCase() == "bacbot") {
                commandFactory.ProcessCommand();
            }
        });
    }

    voice() {
        this.logger.Warn("Not Implemented Exception");
    }
}

module.exports = () => {
    var bacBot = new BacBot();
    bacBot.message();
    bacBot.voice();
    bacBot.ready();
}